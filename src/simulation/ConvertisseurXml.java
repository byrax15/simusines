/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: ConvertisseurXml.java
Date créé: 2021-05-15
Date dern. modif. 2021-05-31
*******************************************************/

package simulation;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.SwingWorker;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import simulation.connecteurs.Chemin;
import simulation.connecteurs.Connecteur;
import simulation.usines.Entrepot;
import simulation.usines.Usine;
import simulation.usines.UsineAile;
import simulation.usines.UsineAssemblage;
import simulation.usines.UsineMatiere;
import simulation.usines.UsineMoteur;

/**
 * Convertisseur de fichier XML asynchrone
 * 
 * @author Vincent Alix-Joly
 */
public class ConvertisseurXml extends SwingWorker<Object, String> implements Observable {

    /**
     * Conserve le nom des types d'usines tels qu'ils apparaissent dans le fichier
     * XML et les lie aux types tels que déclarés dans le code
     */
    private static Map<String, Class<?>> TypesUsine = instancierTypesUsine();

    /**
     * fichier XML à convertir
     */
    private File fichXml;
    /** usines qui peupleront la simulation */
    private LinkedList<Usine> usines = new LinkedList<Usine>();
    /** connecteurs qui peupleront la simulation */
    private LinkedList<Connecteur> connecteurs = new LinkedList<Connecteur>();
    /**
     * combinaison des usines et des connecteurs, plus facile à manipuler par la
     * simulation
     */
    private LinkedList<Synchronisable> synchronisables = new LinkedList<Synchronisable>();

    /** dicte le moment où la simulation peut accéder aux synchronisables */
    private boolean ready = false;

    /**
     * permet de vérifier si la conversion est complète
     * 
     * @return vrai si la sim. est prête, faux sinon.
     */
    public boolean isReady() {
        return ready;
    }

    /**
     * Instancie un convertisseur de fichier XML
     * 
     * @param createur Observateur qui demande la conversion et qui sera averti de
     *                 sa complétion
     * @param fichXml  fichier à convertir
     */
    public ConvertisseurXml(Observateur createur, File fichXml) {
        if (fichXml == null)
            throw new IllegalArgumentException("fichXml ne peut être null");
        this.fichXml = fichXml;
        ajouterObservateur(createur);
    }

    /** exécuteur de la conversion */
    @Override
    protected Object doInBackground() {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc;
            doc = db.parse(fichXml);
            doc.getDocumentElement().normalize();

            NodeList metaNodes = doc.getElementsByTagName(BalisesRacine.metadonnees.toString());
            for (int i = 0; i < metaNodes.getLength(); i++) {
                Node m = metaNodes.item(i);

                NodeList usineNodes = m.getChildNodes();
                for (int j = 0; j < usineNodes.getLength(); j++) {
                    Node u = usineNodes.item(j);

                    if (u.getNodeName().equals(BalisesMeta.usine.toString()) && u.hasAttributes()) {
                        var type = trouverTypeUsine(u);
                        Usine originelle = (Usine) type.getConstructor(Node.class).newInstance(u);
                        originelle.setOriginelle();
                    }
                }
            }

            NodeList simNodes = doc.getElementsByTagName(BalisesRacine.simulation.toString());
            for (int i = 0; i < simNodes.getLength(); i++) {
                Node s = simNodes.item(i);
                NodeList simChildren = s.getChildNodes();

                for (int j = 0; j < simChildren.getLength(); j++) {
                    Node n = simChildren.item(j);
                    if (n.getNodeName().equals(BalisesSim.usine.toString()))
                        clonerUsine(n);
                    else if (n.getNodeName().equals(BalisesSim.chemins.toString()))
                        instancierChemins(n);
                }
            }

            for (Usine u : usines) {
                for (Connecteur c : connecteurs) {
                    if (c.tryAddUsineDebut(u))
                        u.setConnExtrant(c);
                    c.tryAddUsineFin(u);
                }
            }

            ready = true;
            synchronisables.addAll(connecteurs);
            synchronisables.addAll(usines);

            notifierObservateurs(GuiEvents.FICHIER_CONVERTI.toString(), null, "converted " + fichXml.getName());
        } catch (SAXException | IOException | ParserConfigurationException | InstantiationException
                | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
                | SecurityException e) {
            notifierObservateurs(GuiEvents.FICHIER_ERREUR.toString(), null, e);
        }

        return null;
    }

    /**
     * crée les chemins à partir d'un noeud
     * 
     * @param n noeud décrivant les chemins à instancier
     */
    private void instancierChemins(Node n) {
        NodeList chemins = n.getChildNodes();

        for (int i = 0; i < chemins.getLength(); i++) {
            Node c = chemins.item(i);

            if (c.getNodeName().equals(BalisesSim.chemin.toString()))
                connecteurs.add(new Chemin(c));
        }
    }

    /**
     * clone, à partir des usines de configuration originelles, les instances
     * d'usines qui peupleront la simulation
     * 
     * @param s node listant les usines à cloner à leurs caractéristiques
     *          non-communes
     * @throws NoSuchMethodException
     * @throws SecurityException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     * @throws InstantiationException
     * @see Usine.getOriginelle()
     */
    private void clonerUsine(Node s) throws NoSuchMethodException, SecurityException, IllegalAccessException,
            IllegalArgumentException, InvocationTargetException, InstantiationException {
        Class<?> type = trouverTypeUsine(s);
        Usine u = (Usine) type.getConstructor(new Class<?>[0]).newInstance(new Object[0]);
        type.getMethod("copierOriginelle", Usine.class).invoke(null, u);
        u.placerSimulation(s);
        usines.add(u);
    }

    /**
     * trouve à quelle classe d'usine la node fait allusion
     * 
     * @param node contient un type d'usine qui doit être interprêté
     * @return la Class d'usine retrouvée
     */
    private Class<?> trouverTypeUsine(Node node) {
        NamedNodeMap attr = node.getAttributes();
        Node typeAttr = attr.getNamedItem(AttributsCommuns.type.toString());

        if (typeAttr != null) {
            Class<?> type = TypesUsine.get(typeAttr.getNodeValue());

            if (type != null) {
                return type;
            } else
                throw new IllegalArgumentException(String.format(
                        "La node XML: %s tente de configurer autre chose qu'un type d'usine. Les types valide sont: %s.",
                        node, getTypesUsinesKeyString()));
        }
        return null;
    }

    
    /** 
     * @param o
     */
    @Override
    public void ajouterObservateur(Observateur o) {
        addPropertyChangeListener(o);
    }

    
    /** 
     * @param o
     */
    @Override
    public void retirerObservateur(Observateur o) {
        removePropertyChangeListener(o);
    }

    
    /** 
     * @param nomPropriete
     * @param vielleVal
     * @param nouvVal
     */
    @Override
    public void notifierObservateurs(String nomPropriete, Object vielleVal, Object nouvVal) {
        firePropertyChange(nomPropriete, vielleVal, nouvVal);
    }

    /** balises du plus haut niveau */
    private static enum BalisesRacine {
        metadonnees, simulation
    }

    /**
     * liste des balises pouvant se trouver sous la balise de plus haut niveau
     * "metadonnees"
     */
    private static enum BalisesMeta {
        usine
    }

    /**
     * liste des balises pouvant se trouver sous la balise de plus haut niveau
     * "simulation"
     */
    private static enum BalisesSim {
        usine, chemins, chemin
    }

    /**
     * attributs XML se retrouvant à tous les niveaux de la hiérarchie
     */
    private static enum AttributsCommuns {
        type
    }

    /**
     * instancie les associations de chaines de types et de classes d'usines
     * @return la carte des associations complète
     * @see TypesUsines
     */
    private static Map<String, Class<?>> instancierTypesUsine() {
        var types = new HashMap<String, Class<?>>(5);
        types.put("usine-matiere", UsineMatiere.class);
        types.put("usine-moteur", UsineMoteur.class);
        types.put("usine-aile", UsineAile.class);
        types.put("usine-assemblage", UsineAssemblage.class);
        types.put("entrepot", Entrepot.class);
        return types;
    }

    /** 
     * retourne la liste des types d'usines valides sous la forme d'une String
     * @return l'aggrégation de types de strings valides
     */
    private static String getTypesUsinesKeyString() {
        StringBuilder output = new StringBuilder();
        TypesUsine.keySet().forEach(x -> output.append(x));
        return output.toString();
    }

    /**
     * récupère la liste des synchronisables si elle est prête
     * @return la liste des synchronisables ou null si la liste n'est pas encore prête
     */
    public List<Synchronisable> getSynchronisables() {
        if (!ready)
            return null;
        else
            return synchronisables;
    }
}
