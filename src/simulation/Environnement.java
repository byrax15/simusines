/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: Environnement.java
Date créé: 2021-05-05
Date dern. modif. 2021-06-06
*******************************************************/

package simulation;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import simulation.usines.UsineEvents;
import simulation.usines.strategies.Vendeur;
import simulation.usines.strategies.VendeurAleatoire;
import simulation.usines.strategies.VendeurIntervalle;

/**
 * Contrôle l'ensemble de la simulation : dicte quand rafraîchir l'affichage et
 * coordone les tours.
 */
public class Environnement implements Observable, Observateur {
	/** mettre à faux pour mettre fin au fil d,exécution des tours */
	private boolean actif = true;
	/** nombre maximal d'essais de récupération des synchronisables */
	private static int MAX_RETRY = 10;
	/** nombres de phases par tour */
	private static int NOMBRE_PHASES = 2;
	private static int DELAI = 3;

	private ConvertisseurXml cXml;
	/**
	 * lie chaque synchr. avec un booléen qui conserve s'il a terminé d'exécuter la
	 * phase en cours
	 */
	private HashMap<Synchronisable, Boolean> synchronisables = new HashMap<Synchronisable, Boolean>();

	/** fil qui exécute les tours de simulation */
	private Thread enExec;
	/** thread pool exécutant les phases de chaque synchronisable en parallèle */
	private ExecutorService pool = Executors.newFixedThreadPool(10);

	/** observable permettant de communiquer avec l'interface graphique */
	private PropertyChangeSupport obsGui = new PropertyChangeSupport(this);
	/** observable permettant de communiquer avec les synchronisables */
	private PropertyChangeSupport obsSim = new PropertyChangeSupport(this);

	/** stratégie de vente sélectionnée */
	private Vendeur vendeur = new VendeurIntervalle();

	/**
	 * méthode exécutée par le fil de simulation
	 * 
	 * @see enExec
	 */
	protected void boucleTour() {

		try {
			while (actif) {
				for (int i = 0; i < NOMBRE_PHASES; i++) {
					Thread.sleep(DELAI);
					executerPhase(i);
					attendreFinPhase();
					preparerProchainePhase();
					notifierObservateurs(GuiEvents.SIM_DESSINER.toString(), null, "redessiner");
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/** récupère les synchronisables auprès du convertisseur XML, s'il est prêt */
	private void recuperationSynchronisables() {
		try {
			if (enExec != null)
				enExec.interrupt();

			for (int i = 0; !cXml.isReady(); i++) {
				if (i > MAX_RETRY)
					throw new Exception("Dépassé le nombre de tentatives d'accès au fichier converti");
			}

			synchronisables = toCompletionMap(cXml.getSynchronisables());
			if (synchronisables == null || synchronisables.size() == 0)
				throw new Exception("Liste d'éléments de simulation nulle ou vide");

			for (Synchronisable s : synchronisables.keySet()) {
				ajouterObservateur(s);
				notifierObservateurs(GuiEvents.SIM_AJOUTER.toString(), null, s);
			}
			notifierObservateurs(UsineEvents.CHANGE_VENDEUR.toString(), null, vendeur);

			actif = true;
			enExec = new Thread(new Runnable() {

				@Override
				public void run() {
					boucleTour();
				}
			});
			enExec.start();

			notifierObservateurs(GuiEvents.SIM_DESSINER.toString(), null, "redessiner");
		} catch (Exception e) {
			System.err.println(e);
		}
	}

	/**
	 * itère sur les synchronisables et réinitialise leur état de complétion de
	 * phase
	 * 
	 * @see synchronisables
	 */
	private void preparerProchainePhase() {
		for (Synchronisable s : synchronisables.keySet()) {
			synchronisables.put(s, false);
		}
	}

	/**
	 * fait attendre le fil de simulation jusqu'à la complétion de la phase de tous
	 * les synchronisables
	 */
	private void attendreFinPhase() {

		while (true) {
			boolean fini = true;

			for (Boolean b : synchronisables.values()) {
				fini = fini && b;
			}

			if (fini)
				break;
		}
	}

	/**
	 * démarre un fil pour chaque synchronisable
	 * 
	 * @param numPhase
	 */
	private void executerPhase(int numPhase) {
		for (Synchronisable s : synchronisables.keySet()) {
			Runnable executeur = new Runnable() {
				@Override
				public void run() {
					s.executerPhaseTour(numPhase);
					synchronisables.put(s, true);
				}
			};
			pool.execute(executeur);
		}
	}

	/**
	 * transforme un liste de synchronisables et les lie avec un boolean
	 * 
	 * @param synchronisables à convertir
	 * @return Map convertie
	 * @see synchronisables
	 */
	private HashMap<Synchronisable, Boolean> toCompletionMap(List<Synchronisable> synchronisables) {
		HashMap<Synchronisable, Boolean> output = new HashMap<Synchronisable, Boolean>(synchronisables.size());

		for (Synchronisable s : synchronisables) {
			output.put(s, false);
		}

		return output;
	}

	/**
	 * retire tous les objets de la simulation des observables à avertir
	 */
	private void retirerObservateursSynchronisables() {
		for (PropertyChangeListener o : obsSim.getPropertyChangeListeners()) {
			retirerObservateur((Observateur) o);
		}
	}

	/**
	 * change la stratégie de vente de l'environnement et averti les synchronisables
	 * du changement;
	 * 
	 * @param nomNouvVendeur nom de la nouvelle stratégie
	 */
	private void changerVendeurs(Object nomNouvVendeur) {
		Vendeur v;
		if (nomNouvVendeur.equals(NomsVendeurs.ALEATOIRE.getDisplayName()))
			v = new VendeurAleatoire();
		else
			v = new VendeurIntervalle();

		if (v.getClass() != vendeur.getClass()) {
			notifierObservateurs(UsineEvents.CHANGE_VENDEUR.toString(), null, v);
			vendeur = v;
		}
	}

	
	/** 
	 * @param evt
	 * @return boolean
	 */
	private boolean isFileSelectEventValid(PropertyChangeEvent evt) {
		return evt.getPropertyName().equals(GuiEvents.FICHIER_CHOISI.toString()) && evt.getNewValue() instanceof File;
	}

	
	/** 
	 * @param o
	 */
	@Override
	public void ajouterObservateur(Observateur o) {
		if (o instanceof Synchronisable)
			obsSim.addPropertyChangeListener(o);
		else
			obsGui.addPropertyChangeListener(o);
	}

	
	/** 
	 * @param o
	 */
	@Override
	public void retirerObservateur(Observateur o) {
		if (o instanceof Synchronisable)
			obsSim.removePropertyChangeListener(o);
		else
			obsGui.removePropertyChangeListener(o);
	}

	
	/** 
	 * @param nomPropriete
	 * @param vielleVal
	 * @param nouvVal
	 */
	@Override
	public void notifierObservateurs(String nomPropriete, Object vielleVal, Object nouvVal) {

		for (UsineEvents u : UsineEvents.values()) {
			if (nomPropriete.equals(u.toString())) {
				obsSim.firePropertyChange(nomPropriete, vielleVal, nouvVal);
				return;
			}
		}

		obsGui.firePropertyChange(nomPropriete, vielleVal, nouvVal);
	}

	
	/** 
	 * @param evt
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (isFileSelectEventValid(evt)) {
			if (enExec != null)
				actif = false;
			cXml = new ConvertisseurXml(this, (File) evt.getNewValue());
			cXml.execute();
			synchronisables = new HashMap<Synchronisable, Boolean>();
			retirerObservateursSynchronisables();
			notifierObservateurs(GuiEvents.SIM_FLUSH.toString(), null, "flush");

		} else if (evt.getPropertyName().equals(GuiEvents.FICHIER_CONVERTI.toString())) {
			recuperationSynchronisables();

		} else if (evt.getPropertyName().equals(GuiEvents.FICHIER_ERREUR.toString())) {
			System.out.println("Erreur de lecture Xml : " + evt.getNewValue());
		} else if (evt.getPropertyName().equals(GuiEvents.SIM_VENDEUR.toString())) {
			changerVendeurs(evt.getNewValue());
		}
	}

}