/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: FenetrePrincipale.java
Date créé: 2021-05-05
Date dern. modif. 2021-05-31
*******************************************************/

package simulation;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;

import javax.swing.JFrame;

/**
 * base de l'interface de la simulation, conserve tous les menus et permet de
 * relayer les messages entres les différents composants graphiques,
 * l'environnement de simulation et vice-versa
 */
public class FenetrePrincipale extends JFrame implements Observable, Observateur {

	private static final long serialVersionUID = 1L;
	private static final String TITRE_FENETRE = "Laboratoire 1 : LOG121 - Simulation";
	private static final Dimension DIMENSION = new Dimension(700, 700);

	public FenetrePrincipale() {
		PanneauPrincipal panneauPrincipal = new PanneauPrincipal();
		ajouterObservateur(panneauPrincipal);
		MenuFenetre menuFenetre = new MenuFenetre();
		menuFenetre.ajouterObservateur(this);
		add(panneauPrincipal);
		add(menuFenetre, BorderLayout.NORTH);
		// Faire en sorte que le X de la fen�tre ferme la fen�tre
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle(TITRE_FENETRE);
		setSize(DIMENSION);
		// Rendre la fen�tre visible
		setVisible(true);
		// Mettre la fen�tre au centre de l'�cran
		setLocationRelativeTo(null);
		// Emp�cher la redimension de la fen�tre
		setResizable(false);
	}

	
	/** 
	 * @param evt
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals(GuiEvents.SIM_DESSINER.toString()))
			repaint();
		else
			notifierObservateurs(evt.getPropertyName(), evt.getOldValue(), evt.getNewValue());
	}

	
	/** 
	 * @param o
	 */
	@Override
	public void ajouterObservateur(Observateur o) {
		addPropertyChangeListener(o);
	}

	
	/** 
	 * @param o
	 */
	@Override
	public void retirerObservateur(Observateur o) {
		removePropertyChangeListener(o);
	}

	
	/** 
	 * @param nomPropriete
	 * @param vielleVal
	 * @param nouvVal
	 */
	@Override
	public void notifierObservateurs(String nomPropriete, Object vielleVal, Object nouvVal) {
		firePropertyChange(nomPropriete, vielleVal, nouvVal);
	}
}
