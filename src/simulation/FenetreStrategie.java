/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: FenetreStrategie.java
Date créé: 2021-05-05
Date dern. modif. 2021-06-06
*******************************************************/

package simulation;

import java.awt.Dimension;

import javax.swing.JFrame;

/**
 * fenetre de base du selecteur de stratégies de vente. sert de relais entre les
 * boutons de sélection et le menu
 */
public class FenetreStrategie extends JFrame implements Observable {

	private static final long serialVersionUID = 1L;
	private static final String TITRE_FENETRE = "S�lectionnez votre strat�gie de vente";
	private static final Dimension DIMENSION = new Dimension(250, 100);
	private PanneauStrategie panneauStrategie = new PanneauStrategie();

	public FenetreStrategie() {
		add(panneauStrategie);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setTitle(TITRE_FENETRE);
		setSize(DIMENSION);
		setVisible(true);
		setLocationRelativeTo(null);
		setResizable(false);
	}

	
	/** 
	 * @param o
	 */
	@Override
	public void ajouterObservateur(Observateur o) {
		addPropertyChangeListener(o);
	}

	
	/** 
	 * @param o
	 */
	@Override
	public void retirerObservateur(Observateur o) {
		addPropertyChangeListener(o);
	}

	
	/** 
	 * @param nomPropriete
	 * @param vielleVal
	 * @param nouvVal
	 */
	@Override
	public void notifierObservateurs(String nomPropriete, Object vielleVal, Object nouvVal) {
		firePropertyChange(nomPropriete, vielleVal, nouvVal);
	}
}
