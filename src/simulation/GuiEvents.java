/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: GuiEvents.java
Date créé: 2021-05-15
Date dern. modif. 2021-06-06
*******************************************************/

package simulation;

/**
 * énumération des évènements propres à l'interface graphiques, notamment la
 * sélection de fichier et la manipulation des éléments à dessiner
 */
public enum GuiEvents {
    FICHIER_CHOISI, FICHIER_CONVERTI, FICHIER_ERREUR, SIM_DESSINER, SIM_AJOUTER, SIM_RETIRER, SIM_FLUSH, SIM_VENDEUR
}