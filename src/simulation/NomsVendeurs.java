/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: NomsVendeurs.java
Date créé: 2021-06-06
Date dern. modif. 2021-06-06
*******************************************************/

package simulation;

/**
 * énumération des noms de stratégies à afficher dans le menu de sélection de
 * stratégie
 */
public enum NomsVendeurs {
    INTERVALLE("Intervalle"), ALEATOIRE("Aléatoire");

    private String displayName;

    private NomsVendeurs(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
