/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: Observable.java
Date créé: 2021-05-15
Date dern. modif. 2021-06-06
*******************************************************/

package simulation;

/**
 * interface indiquant que l'objet peut être observé, et envoie des mises à jour
 * aux inscrits
 */
public interface Observable {
    /**
     * ajoute un observateur à la liste des objets qui surveillent le présent objet
     * 
     * @param o l'observateur à ajouter
     */
    void ajouterObservateur(Observateur o);

    /**
     * retire un observateur de la liste des objets qui surveillent le présent objet
     * 
     * @param o l'observateur à retirer
     */
    void retirerObservateur(Observateur o);

    /**
     * avertit tous les surveillants qu'une propriété à changéchangé
     * 
     * @param nomPropriete nom de la propriété qui a changé
     * @param vielleVal    ancienne valeur de la propriété
     * @param nouvVal      valeur qui remplace l'ancienne
     */
    void notifierObservateurs(String nomPropriete, Object vielleVal, Object nouvVal);
}
