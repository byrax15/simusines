/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: Observateur.java
Date créé: 2021-05-15
Date dern. modif. 2021-06-06
*******************************************************/

package simulation;

import java.beans.PropertyChangeListener;

/** surnom pour l'interface PropertyChangeListener */
public interface Observateur extends PropertyChangeListener {
}
