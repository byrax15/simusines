/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: PanneauPrincipal.java
Date créé: 2021-05-05
Date dern. modif. 2021-06-06
*******************************************************/

package simulation;

import java.awt.Graphics;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Comparator;

import javax.swing.JPanel;

/** panneau affichant la simulation : usines, connecteurs, composants */
public class PanneauPrincipal extends JPanel implements Observateur {

	private static final long serialVersionUID = 1L;

	/**
	 * liste des synchronisables, classée par ordre croissant de priorité
	 * d'affichage @see Synchronisable.getPrioriteDessin()
	 */
	private ArrayList<Synchronisable> synchros = new SortedSynchronisables();

	
	/** 
	 * @param g
	 */
	@Override
	public void paint(Graphics g) {
		super.paint(g);

		for (Synchronisable d : synchros) {
			d.dessiner(g);
		}
	}

	
	/** 
	 * @param evt
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals(GuiEvents.SIM_AJOUTER.toString())
				&& evt.getNewValue() instanceof Synchronisable)
			synchros.add((Synchronisable) evt.getNewValue());
		else if (evt.getPropertyName().equals(GuiEvents.SIM_RETIRER.toString())
				&& evt.getNewValue() instanceof Synchronisable)
			synchros.remove((Synchronisable) evt.getNewValue());
		else if (evt.getPropertyName().equals(GuiEvents.SIM_FLUSH.toString()))
			synchros = new SortedSynchronisables();
	}

	/** collection de synchronisables ordonnes par priorite de dessin */
	private class SortedSynchronisables extends ArrayList<Synchronisable> {
		Comparator<Synchronisable> c = new Comparator<Synchronisable>() {

			@Override
			public int compare(Synchronisable o1, Synchronisable o2) {
				if (o1.getPrioriteDessin() == o2.getPrioriteDessin())
					return 0;
				else if (o1.getPrioriteDessin() < o2.getPrioriteDessin())
					return -1;
				else
					return 1;
			}

		};

		SortedSynchronisables() {
			super();
		}

		@Override
		public boolean add(Synchronisable e) {
			var out = super.add(e);
			this.sort(c);
			return out;
		}
	}

}