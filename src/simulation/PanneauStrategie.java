/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: PanneauStrategie.java
Date créé: 2021-05-05
Date dern. modif. 2021-06-06
*******************************************************/

package simulation;

import java.awt.Component;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.Enumeration;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingUtilities;

/** panneau responsable du sélecteur de stratégie de vente */
public class PanneauStrategie extends JPanel {

	private static final long serialVersionUID = 1L;

	public PanneauStrategie() {

		ButtonGroup groupeBoutons = new ButtonGroup();
		JRadioButton strategie1 = new JRadioButton(NomsVendeurs.INTERVALLE.getDisplayName());
		JRadioButton strategie2 = new JRadioButton(NomsVendeurs.ALEATOIRE.getDisplayName());

		JButton boutonConfirmer = new JButton("Confirmer");

		boutonConfirmer.addActionListener((ActionEvent e) -> {
			Window ancetre = SwingUtilities.getWindowAncestor((Component) e.getSource());
			if (ancetre instanceof Observable)
				((Observable) ancetre).notifierObservateurs(GuiEvents.SIM_VENDEUR.toString(), null,
						getSelectedButtonText(groupeBoutons));
			ancetre.dispose();
		});

		JButton boutonAnnuler = new JButton("Annuler");

		boutonAnnuler.addActionListener((ActionEvent e) -> {
			SwingUtilities.getWindowAncestor((Component) e.getSource()).dispose();
		});

		groupeBoutons.add(strategie1);
		groupeBoutons.add(strategie2);
		add(strategie1);
		add(strategie2);
		add(boutonConfirmer);
		add(boutonAnnuler);

	}

	/**
	 * Retourne le texte du bouton sélectionné dans un groupe de boutons.
	 * 
	 * @param groupeBoutons
	 * @return
	 */
	public String getSelectedButtonText(ButtonGroup groupeBoutons) {
		for (Enumeration<AbstractButton> boutons = groupeBoutons.getElements(); boutons.hasMoreElements();) {
			AbstractButton bouton = boutons.nextElement();
			if (bouton.isSelected()) {
				return bouton.getText();
			}
		}

		return null;
	}

}
