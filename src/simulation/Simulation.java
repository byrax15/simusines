/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: Simulation.java
Date créé: 2021-05-05
Date dern. modif. 2021-05-15
*******************************************************/

package simulation;

/**
 * Classe responsable du lancement de l'application et de son interface
 * graphique
 */
public class Simulation {

	/**
	 * classe lançant l'environnement de simulation, la fenêtre principale et
	 * configurant la communication entre les deux.
	 */
	public static void main(String[] args) {
		Environnement environnement = new Environnement();
		FenetrePrincipale fenetre = new FenetrePrincipale();
		fenetre.ajouterObservateur(environnement);

		environnement.ajouterObservateur(fenetre);
	}

}
