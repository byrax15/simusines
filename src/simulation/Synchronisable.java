/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: Synchronisable.java
Date créé: 2021-05-15
Date dern. modif. 2021-06-06
*******************************************************/

package simulation;

import java.awt.Graphics;

/**
 * interface signifiant que l'objet peut être intégré à la simulation
 * tour-par-tour
 */
public interface Synchronisable extends Observateur {

    /**
     * exécute l'ensemble des tâches que doit accomplir le synchronisable pendant
     * cette phase
     * 
     * @param numPhase numéro de phase à exécuter
     */
    public void executerPhaseTour(int numPhase);

    /**
     * met à jour l'affichage de l'objet
     * 
     * @param g
     */
    public void dessiner(Graphics g);

    /**
     * permet d'obtenir la priorité de dessin de l'objet (la "couche" sur laquelle
     * l'objet est dessiné). Une priorité plus basse signifie que l'objet est
     * dessiné par dessus les autres
     * 
     * @return la priorité de dessin sous forme d'entier
     */
    public int getPrioriteDessin();
}
