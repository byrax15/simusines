/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: Aile.java
Date créé: 2021-05-15
Date dern. modif. 2021-06-06
*******************************************************/

package simulation.composants;

import java.awt.Point;
import java.net.URL;

import javax.swing.ImageIcon;

/** sous classe de composant représentant les ailes d'un avion */
public class Aile extends Composant {

    private static URL urlIcone = Composant.class.getClassLoader().getResource("ressources/aile.png");
    private static ImageIcon icone = new ImageIcon(urlIcone);

    /**
     * construit un nouvelle aile en appelant le constructeur de Composant avec une
     * position nulle et un quantité de 1
     */
    public Aile() {
        this(null, 1);
    }

    /**
     * Construit un nouvelle aile en appelant le constructeur de composant pour une
     * position et une quantité donnée
     * 
     * @param position la position actuelle du composant
     * @param quantite le nombre de ce composant utilisable
     */
    public Aile(Point position, int quantite) {
        super(position, quantite);
    }

    @Override
    public Composant clone() {
        Composant c = new Aile();
        if (position != null)
            c.position = new Point(position);
        c.quantite = quantite;
        return c;
    }

    @Override
    public ImageIcon getIcone() {
        return icone;
    }
}
