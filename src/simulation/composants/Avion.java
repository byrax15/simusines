/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: Avion.java
Date créé: 2021-05-15
Date dern. modif. 2021-06-06
*******************************************************/

package simulation.composants;

import java.awt.Point;
import java.net.URL;

import javax.swing.ImageIcon;

/** sous-classe de composant representant l'avion complet */
public class Avion extends Composant {

    private static URL urlIcone = Composant.class.getClassLoader().getResource("ressources/avion.png");
    private static ImageIcon icone = new ImageIcon(urlIcone);

    /**
     * construit un avion ayant une position nulle et une quantité de 1
     */
    public Avion() {
        this(null, 1);
    }

    /**
     * Construit un nouvel avion en appelant le constructeur de composant pour une
     * position et une quantité donnée
     * 
     * @param position la position actuelle du composant
     * @param quantite le nombre de ce composant utilisable
     */
    public Avion(Point position, int quantite) {
        super(position, quantite);
    }

    @Override
    public Composant clone() {
        Composant c = new Avion();
        if (position != null)
            c.position = new Point(position);
        c.quantite = quantite;
        return c;
    }

    public ImageIcon getIcone() {
        return icone;
    }
}
