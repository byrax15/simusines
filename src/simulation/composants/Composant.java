/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: Composant.java
Date créé: 2021-05-15
Date dern. modif. 2021-06-06
*******************************************************/

package simulation.composants;

import java.awt.Graphics;
import java.awt.Point;

import javax.swing.ImageIcon;

/**
 * classe représentant les matériaux nécessaires à la construction d'un avion et
 * de ses constituants
 */
public abstract class Composant implements Cloneable {

    /** permet de calculer les collisions avec les usines et de dessiner l'icône */
    protected Point position;
    /** quantité du composable disponible à l'utilisation */
    protected int quantite;

    /**
     * construit un composant ayant la position et la quantité données
     * 
     * @param position endroit où se trouve le composant. Peut être nulle.
     * @param quantite quantité de composant utilisable pour la production.
     */
    protected Composant(Point position, int quantite) {
        this.position = position != null ? new Point(position) : null;
        this.quantite = quantite;
    }

    /**
     * Crée une copie du composant, la quantité est la même et la position est une
     * nouvelle instance de Point contenant une copie des coordonnées
     */
    @Override
    public abstract Composant clone();

    public int getQuantite() {
        return quantite;
    }

    /**
     * change la quantité de composant
     * 
     * @param quantite la nouvelle quantité
     */
    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    /**
     * ajoute une quantité au composant
     * 
     * @param ajout la quantité ajoutée
     */
    public void ajouterQuantite(int ajout) {
        quantite += ajout;
    }

    /**
     * enlève une quantité au composant
     * 
     * @param retrait la quantité retirée
     */
    public void retirerQuantite(int retrait) {
        quantite -= retrait;
        if (quantite < 0)
            quantite = 0;
    }

    /**
     * change la position du composant
     * 
     * @param position la nouvelle position
     */
    public void setPosition(Point position) {
        this.position = position;
    }

    /**
     * obtient la position du composant
     * 
     * @return la position du composant
     */
    public Point getPosition() {
        return position;
    }

    /** ajoute un déplacement en x et en y à la position du composant */
    public void avancer(int deplX, int deplY) {
        position.x += deplX;
        position.y += deplY;
    }

    /**
     * dessine le composant dans l'interface graphique
     * 
     * @param g l'objet Graphics du Component awt sur lequel est dessiné le
     *          Composant
     */
    public void dessiner(Graphics g) {
        var icone = getIcone();
        if (position != null) {
            if (icone != null) {
                var image = getIcone().getImage();
                Point offset = new Point(position.x - image.getWidth(null) / 2, position.y - image.getHeight(null) / 2);
                g.drawImage(icone.getImage(), offset.x, offset.y, null);
            } else {
                Point offset = new Point(position.x - 10 / 2, position.y - 10 / 2);
                g.fillRect(offset.x, offset.y, 10, 10);
            }
        }
    }

    /**
     * obtient l'icône qui représente le composant
     * 
     * @return icone qui représente ce composant
     */
    public abstract ImageIcon getIcone();
}
