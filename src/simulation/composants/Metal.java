/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: Metal.java
Date créé: 2021-05-15
Date dern. modif. 2021-06-06
*******************************************************/

package simulation.composants;

import java.awt.Point;
import java.net.URL;

import javax.swing.ImageIcon;

/** composant représentant la toute base de la chaine de montage */
public class Metal extends Composant {

    private static URL urlIcone = Composant.class.getClassLoader().getResource("ressources/metal.png");
    private static ImageIcon icone = new ImageIcon(urlIcone);

    /** construit un nouveau métal ayant une position nulle et une quantité de 1 */
    public Metal() {
        this(null, 1);
    }

    /**
     * Construit un nouveau métal en appelant le constructeur de composant pour une
     * position et une quantité donnée
     * 
     * @param position la position actuelle du composant
     * @param quantite le nombre de ce composant utilisable
     */
    public Metal(Point position, int quantite) {
        super(position, quantite);
    }

    @Override
    public Composant clone() {
        Composant c = new Metal();
        if (position != null)
            c.position = new Point(position);
        c.quantite = quantite;
        return c;
    }

    public ImageIcon getIcone() {
        return icone;
    }
}
