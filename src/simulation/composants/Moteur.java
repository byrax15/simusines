/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: Moteur.java
Date créé: 2021-05-15
Date dern. modif. 2021-06-06s
*******************************************************/

package simulation.composants;

import java.awt.Point;
import java.net.URL;

import javax.swing.ImageIcon;

/** composant intermediaire de la chaine */
public class Moteur extends Composant {

    private static URL urlIcone = Composant.class.getClassLoader().getResource("ressources/moteur.png");
    private static ImageIcon icone = new ImageIcon(urlIcone);

    /**
     * Construit un nouveau moteur ayant une position nulle et une quantité de 1
     */
    public Moteur() {
        this(null, 1);
    }

    /**
     * Construit un nouveau moteur en appelant le constructeur de composant pour une
     * position et une quantité donnée
     * 
     * @param position la position actuelle du composant
     * @param quantite le nombre de ce composant utilisable
     */
    public Moteur(Point position, int quantite) {
        super(position, quantite);
    }

    @Override
    public Composant clone() {
        Composant c = new Moteur();
        if (position != null)
            c.position = new Point(position);
        c.quantite = quantite;
        return c;
    }

    public ImageIcon getIcone() {
        return icone;
    }

}
