/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: Chemin.java
Date créé: 2021-05-15
Date dern. modif. 2021-06-06
*******************************************************/

package simulation.connecteurs;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import simulation.composants.Composant;
import simulation.usines.Usine;

/** objet reliant deux usines et qui permet de transporter des composants */
public class Chemin extends Connecteur {

    /**
     * id de l'usine de départ, permet d'y lier la bonne instance d'usine lors de la
     * configuration de la simulation
     */
    private int usineDebutId;
    /**
     * id de l'usine de fin, permet d'y lier la bonne instance d'usine lors de la
     * configuration de la simulation
     */
    private int usineFinId;

    /**
     * usine de départ et qui utilise l'instance de connecteur pour effectuer des
     * livraisons
     */
    private Usine usineDebut;
    /** usine de fin et qui reçoit des composants */
    private Usine usineFin;

    private int prioriteDessin = 0;
    /** composants en transit sur le présent chemin */
    private ArrayList<Composant> composants = new ArrayList<Composant>();

    /**
     * crée un chemin à partir d'un node XML contenant l'id des usines de départ et
     * de fin
     * 
     * @param s node XML dont ont extrait l'information
     */
    public Chemin(Node s) {
        NamedNodeMap attr = s.getAttributes();

        var de = attr.getNamedItem(AttributsXml.de.toString());
        var vers = attr.getNamedItem(AttributsXml.vers.toString());

        if (de != null)
            usineDebutId = Integer.parseInt(de.getTextContent());
        if (de != null)
            usineFinId = Integer.parseInt(vers.getTextContent());
    }

    /**
     * obtient de départ du chemin
     * 
     * @return la position de l'usine de départ, si elle n'est pas nulle
     */
    public Point getPositionDebut() {
        return usineDebut != null ? usineDebut.getPosition() : null;
    }

    /**
     * obtient la position de la fin du chemin
     * 
     * @return la position de l'usine de fin, si elle n'est pas nulle
     */
    public Point getPositionFin() {
        return usineFin != null ? usineFin.getPosition() : null;
    }

    @Override
    public boolean tryAddUsineDebut(Usine u) {
        int id = u.getId();
        if (id == usineDebutId) {
            usineDebut = u;
            return true;
        }
        return false;
    }

    @Override
    public boolean tryAddUsineFin(Usine u) {
        int id = u.getId();
        if (id == usineFinId) {
            usineFin = u;
            return true;
        }
        return false;
    }

    @Override
    public int getPrioriteDessin() {
        return prioriteDessin;
    }

    @Override
    public void dessiner(Graphics g) {
        g.setColor(Color.BLACK);
        Point debut = getPositionDebut();
        Point fin = getPositionFin();
        g.drawLine(debut.x, debut.y, fin.x, fin.y);
        for (int i = composants.size() - 1; i >= 0; i--) {
            composants.get(i).dessiner(g);
        }
    }

    @Override
    public void executerPhaseTour(int numPhase) {
        if (numPhase == 0) {
            Point debut = getPositionDebut();
            Point fin = getPositionFin();

            int deplX = fin.x - debut.x;
            int deplY = fin.y - debut.y;

            int signeX = (int) Math.signum(deplX);
            int signeY = (int) Math.signum(deplY);

            for (Composant composant : composants) {
                var pos = composant.getPosition();
                composant.setPosition(new Point(pos.x + signeX, pos.y + signeY));
                // composant.avancer(signeX, signeY);
            }

        } else if (numPhase == 1) {

            for (int i = composants.size() - 1; i >= 0; i--) {
                var c = composants.get(i);
                if (c.getPosition() != null && c.getPosition().equals(getPositionFin())) {
                    completerLivraison(c);
                    modifierComposants(CommandesModifComposants.RETIRER, c);
                }
            }
        }
    }

    @Override
    public void debuterLivraison(Composant c) {
        modifierComposants(CommandesModifComposants.AJOUTER, c);
        c.setPosition(getPositionDebut());
    }

    /**
     * transmet le stock arrivé à destination à l'usine de fin
     * 
     * @param c composant à livrer
     */
    public void completerLivraison(Composant c) {
        usineFin.ajouterStock(c);
    }

    /**
     * permet de modifier la liste des composants de manière synchrone
     * 
     * @param commande AJOUTER ou RETIRER
     * @param c        composant qu'on souhaite altérer dans la liste
     */
    public synchronized void modifierComposants(CommandesModifComposants commande, Composant c) {
        if (commande == CommandesModifComposants.AJOUTER) {
            composants.add(c);
        } else if (commande == CommandesModifComposants.RETIRER) {
            composants.remove(c);
        }
    }

    /** noms des nodes d'attributs xml apparaissant dans les nodes de chemin */
    private static enum AttributsXml {
        de, vers
    }

    @Override
    public Usine getUsineFin() {
        return usineFin;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
    }

    /** énumération des commandes de modification de la liste des composants */
    private enum CommandesModifComposants {
        AJOUTER, RETIRER
    }
}
