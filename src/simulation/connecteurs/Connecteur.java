/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: Connecteur.java
Date créé: 2021-05-15
Date dern. modif. 2021-06-06
*******************************************************/

package simulation.connecteurs;

import java.awt.Point;

import simulation.Synchronisable;
import simulation.composants.Composant;
import simulation.usines.Usine;

/** abstraction des éventuels types de connecteurs pouvant lier des usines */
public abstract class Connecteur implements Synchronisable {
    /**
     * essaie de lier une usine au début du connecteur
     * 
     * @param u l'usine que l'on tente d'assigner au début
     * @return vrai si l'assignation a fonctionnée, faux dans le cas contraire
     */
    public abstract boolean tryAddUsineDebut(Usine u);

    /**
     * essaie de lier une usine à la fin du connecteur
     * 
     * @param u l'usine que l'on tente d'assigner à la fin
     * @return vrai si l'assignation a fonctionnée, faux dans le cas contraire
     */
    public abstract boolean tryAddUsineFin(Usine u);

    /**
     * retourne le point de départ du connecteur, s'il existe
     * 
     * @return le Point de début du Connecteur
     */
    public abstract Point getPositionDebut();

    /**
     * mets un composant en route vers l'usine de fin
     * 
     * @param composant le omposant que l'on souhaite livrer
     */
    public abstract void debuterLivraison(Composant composant);

    /**
     * retourne l'usine assignée à la fin du connecteur
     * 
     * @return l'usine de fin, si elle existe
     */
    public abstract Usine getUsineFin();
}
