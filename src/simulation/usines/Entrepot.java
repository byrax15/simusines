/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: Entrepot.java
Date créé: 2021-05-15
Date dern. modif. 2021-06-06
*******************************************************/

package simulation.usines;

import java.lang.reflect.InvocationTargetException;

import org.w3c.dom.DOMException;
import org.w3c.dom.Node;

import simulation.usines.strategies.AffichageStock;
import simulation.usines.strategies.VendeurIntervalle;

/** sous-classe d'usine qui stocke des avions et tente de les vendre */
public class Entrepot extends Usine {

    public static Entrepot originel;

    /**
     * construit un entrepot avec le constructeur d'usine vide, mais changes les
     * stratégies par défaut par une stratégie d'affichage par stock et un
     * estratégie d'usine de Vendeur par intervalles
     */
    public Entrepot() {
        super();
        strategieAffichage = new AffichageStock();
        strategieUsine = new VendeurIntervalle();
    }

    /**
     * construit un entrepot avec le constructeur d'usine avec node, une stratégie
     * d'affichage par stock et une stratégie d'usine de Vendeur par intervalles
     * 
     * @param nodeUsine
     * @throws NumberFormatException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     * @throws SecurityException
     * @throws ClassNotFoundException
     * @throws DOMException
     */
    public Entrepot(Node nodeUsine)
            throws NumberFormatException, InstantiationException, IllegalAccessException, IllegalArgumentException,
            InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException, DOMException {
        super(nodeUsine, new VendeurIntervalle(), new AffichageStock());
    }

    @Override
    public void setOriginelle() {
        originel = this;
    }

    
    /** 
     * @return Usine
     */
    @Override
    public Usine getOriginelle() {
        return originel;
    }
}
