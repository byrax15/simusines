/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: IconesProgression.java
Date créé: 2021-05-15
Date dern. modif. 2021-06-06
*******************************************************/

package simulation.usines;

import java.util.HashMap;

import javax.swing.ImageIcon;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.NamedNodeMap;

/** collectiopn associant une fraction sous forme de String à un ImageIcon */
class IconesProgression extends HashMap<String, ImageIcon> {
    /** construit un IconesProgression vide */
    public IconesProgression() {
        super();
    }

    /**
     * construit un IconesProgression à partir d'une node de configuration XML
     * 
     * @param p node xml dont la config. sera extraite
     */
    public IconesProgression(Node p) {
        NodeList iconeNodes = p.getChildNodes();

        for (int i = 0; i < iconeNodes.getLength(); i++) {
            Node ico = iconeNodes.item(i);
            NamedNodeMap attr = ico.getAttributes();

            if (attr == null)
                continue;
            for (int j = 0; j < attr.getLength(); j++) {
                Node type = attr.getNamedItem(AttributsXML.type.toString());
                Node path = attr.getNamedItem(AttributsXML.path.toString());

                if (type != null && path != null)
                    ajouterIcone(type.getTextContent(), path.getTextContent());
            }
        }
    }

    /**
     * ajoute une fraction et son icone à la collection
     * 
     * @param fracStr clé de l'entrée
     * @param icoPath nom de fichier utilisé pour retrouver l'image depuis le disque
     *                dur
     */
    public void ajouterIcone(String fracStr, String icoPath) {
        put(fracStr, new ImageIcon(icoPath));
    }

    /**
     * retourne l'icone de la collection ayant la fraction la plus petite mais
     * strictement supérieure à la fraction entrée
     * 
     * @param fractionProgression fraction entrée
     * @return icone représentant la progression actuelle
     */
    public ImageIcon getIconeProgression(float fractionProgression) {
        if (fractionProgression < 1 / 3f)
            return get("vide");
        else if (fractionProgression < 2 / 3f)
            return get("un-tiers");
        else if (fractionProgression < 1f)
            return get("deux-tiers");
        else
            return get("plein");
    }

    /**
     * énumération des attributs XML composant les nodes de configuration de cette
     * classe
     */
    private static enum AttributsXML {
        type, path
    }
}