/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: Inventaire.java
Date créé: 2021-05-15
Date dern. modif. 2021-06-06
*******************************************************/

package simulation.usines;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

import org.w3c.dom.DOMException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import simulation.composants.Composant;

/**
 * extension de hashmap spécialisée pour les composants et conservant le mode de
 * stockage
 */
class Inventaire extends HashMap<Composant, String> {

    /** conserve lecomment l'inventaire doit réagir aux nouveaux stocks */
    private String modeStockage;

    /** construit un nouvel inventaire vide */
    public Inventaire() {
        super();
    }

    /**
     * construit un nouvel inventaire en se servant d'une node XML
     * 
     * @param p
     * @throws NumberFormatException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     * @throws SecurityException
     * @throws ClassNotFoundException
     * @throws DOMException
     */
    public Inventaire(Node p)
            throws NumberFormatException, InstantiationException, IllegalAccessException, IllegalArgumentException,
            InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException, DOMException {
        NamedNodeMap attr = p.getAttributes();
        Node type = attr.getNamedItem(AttributsXML.type.toString());
        Node nombre = attr.getNamedItem(AttributsXML.capacite.toString());
        if (nombre == null)
            nombre = attr.getNamedItem(AttributsXML.quantite.toString());

        if (type != null) {
            Composant c = creerComposant(type.getTextContent(),
                    nombre != null ? Integer.parseInt(nombre.getTextContent()) : 1);
            put(c, modeStockage);
            if (nombre != null)
                modeStockage = nombre.getNodeName();
        }
    }

    /**
     * crée un nouveau composant par réflection et l'ajoute à l'inventaire
     * 
     * @param typeComposant
     * @param quantite
     * @return
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     * @throws SecurityException
     * @throws ClassNotFoundException
     */
    private Composant creerComposant(String typeComposant, int quantite)
            throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException,
            NoSuchMethodException, SecurityException, ClassNotFoundException {
        String nomClasse = typeComposant.substring(0, 1).toUpperCase() + typeComposant.substring(1);
        String nomPackage = Composant.class.getPackageName();
        Composant c = (Composant) Class.forName(nomPackage + "." + nomClasse).getConstructor(new Class<?>[0])
                .newInstance(new Object[0]);
        c.setQuantite(quantite);
        return c;
    }

    /**
     * tente d'ajouter un composant à l'inventaire, si l'entree d'inventaire
     * limitante le permet
     * 
     * @param invLimitant entrée de l'inventaire, la clé étant le type de composant
     *                    recherché et la valeur de l'entrée spécifiant les
     *                    restrictions d'ajout
     * @param ajout       quantité de composnt à ajouter au composant retrouvé
     */
    public void ajouterComposant(Inventaire.Entry<Composant, String> invLimitant, int ajout) {
        Composant compExistant = null;
        if (invLimitant == null)
            return;

        for (Composant c : keySet()) {
            if (invLimitant.getKey().getClass() == c.getClass()) {
                compExistant = c;
                break;
            }
        }

        if (compExistant == null) {
            Composant clone = invLimitant.getKey().clone();
            clone.setQuantite(0);
            synchronized (this) {
                put(clone, modeStockage);
            }
            compExistant = clone;
        }

        if (invLimitant.getValue() != null && invLimitant.getValue().equals(AttributsXML.capacite.toString())) {

            if (invLimitant.getKey().getQuantite() > compExistant.getQuantite() + ajout)
                compExistant.ajouterQuantite(ajout);
            else
                compExistant.setQuantite(invLimitant.getKey().getQuantite());
        } else {
            compExistant.ajouterQuantite(ajout);
        }
    }

    /**
     * retourne l'entrée d'inventaire dont la classe correspond à celle du paramètre
     * 
     * @param classeRecherche classe du composant recherché
     * @return le composant trouvé accompagné du mode de stocakge de l'inventaire
     */
    public Inventaire.Entry<Composant, String> getComposant(Class<?> classeRecherche) {
        for (Entry<Composant, String> e : entrySet()) {
            if (e.getKey().getClass() == classeRecherche) {
                e.setValue(modeStockage);
                return e;
            }
        }
        return null;
    }

    /**
     * obtient le mod de stockage de l'inventaire
     * 
     * @return modeStockage
     */
    public String getModeStockage() {
        return modeStockage;
    }

    /**
     * attributs XML faisant parties des nodes de configuration de l'inventaire
     */
    private static enum AttributsXML {
        type, capacite, quantite,
    }
}