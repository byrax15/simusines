/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: Usine.java
Date créé: 2021-05-15
Date dern. modif. 2021-06-06
*******************************************************/

package simulation.usines;

import java.awt.Graphics;
import java.awt.Point;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeSupport;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;

import org.w3c.dom.DOMException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import simulation.Observable;
import simulation.Observateur;
import simulation.Synchronisable;
import simulation.composants.Composant;
import simulation.connecteurs.Connecteur;
import simulation.usines.strategies.StrategieAffichage;
import simulation.usines.strategies.StrategieUsine;
import simulation.usines.strategies.Vendeur;

/**
 * abstraction des différents types d'usines. Une usine tente à chaque tour
 * d'exécuter des actions en fonction de son stock, de ses entrées et de ses
 * sorties, et averti les usines qui la fournissent de son état.
 */
public abstract class Usine implements Synchronisable, Observable {

    /** identifiant de l'usine parmi la simulation */
    protected Integer id;
    protected Point position;

    /** décrit les actions de l'usine à chaque phase */
    protected StrategieUsine strategieUsine;

    /** conserve les composants utilisables par l'usine */
    protected Inventaire stock = new Inventaire();
    /**
     * conserve les composants dont a besoin l'usine pour produire ou la capacité de
     * stockage de chaque composant
     */
    protected Inventaire entrees = new Inventaire();
    /**
     * conserve les composants produits par l'usine lorsque les condiftions propices
     * sont remplies
     */
    protected Inventaire sorties = new Inventaire();

    /** liste des composants actuellements en fabrication */
    protected Composant[] enFabrication;
    /** tours requis pour produire */
    protected int intervalleProduction = 200;
    /** tours de production passés */
    protected int compteurTour = 0;
    /**
     * efficacité de l'usine, ajustée dynamiquement en fonction des usines qui
     * suivent
     */
    protected float tauxExecution = 1;

    protected int prioriteDessin = 10;
    /** dicte les étapes à suivre pour dessiner l'usine sur l'interface */
    protected StrategieAffichage strategieAffichage;
    /** collection d'icones à afficher, utilisée la stratégie d'affichage */
    protected IconesProgression icones = new IconesProgression();

    /** Connecteur servant à expédier les composants vers la prochaine usine */
    private Connecteur connExtrant;

    /** source de mises à jours d'état */
    private Observable prochaineUsine;
    /** vitesse diffusée aux usines qui s'inscrivent au présent observable */
    protected float vitesseDiffusee = 1f;
    /** objet utilisé pour implémenter concrètement le patron observateur */
    protected PropertyChangeSupport obs = new PropertyChangeSupport(this);

    /** construit une usine avec les paramètres par défaut */
    protected Usine() {
    }

    /**
     * construit une usine à partir d'une Node XML
     * 
     * @param nodeUsine node contenant les informations de configuration
     * @throws NumberFormatException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     * @throws SecurityException
     * @throws ClassNotFoundException
     * @throws DOMException
     */
    protected Usine(Node nodeUsine)
            throws NumberFormatException, InstantiationException, IllegalAccessException, IllegalArgumentException,
            InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException, DOMException {
        NodeList proprietes = nodeUsine.getChildNodes();
        for (int i = 0; i < proprietes.getLength(); i++) {
            Node p = proprietes.item(i);

            if (nodeNameEquals(p, BalisesXml.icones))
                icones = new IconesProgression(p);
            else if (nodeNameEquals(p, BalisesXml.entree))
                entrees = new Inventaire(p);
            else if (nodeNameEquals(p, BalisesXml.sortie))
                sorties = new Inventaire(p);
            else if (nodeNameEquals(p, BalisesXml.intervalleProduction))
                setIntervalleProduction(p);
        }
    }

    /**
     * construit une usine à partir d'une Node XML, tout en spécifiant les
     * stratégies voulues par l'implémentation concrète de l'usine
     * 
     * @param nodeUsine          node contenant les informations de configuration
     * @param strategieUsine     actions tour-par-tour voulues
     * @param strategieAffichage instructions d'affichage voulues
     * @throws NumberFormatException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     * @throws SecurityException
     * @throws ClassNotFoundException
     * @throws DOMException
     */
    protected Usine(Node nodeUsine, StrategieUsine strategieUsine, StrategieAffichage strategieAffichage)
            throws NumberFormatException, InstantiationException, IllegalAccessException, IllegalArgumentException,
            InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException, DOMException {
        this(nodeUsine);
        this.strategieAffichage = strategieAffichage;
        this.strategieUsine = strategieUsine;
    }

    /**
     * obtient la stratégie d'usine en place
     * 
     * @return la stratégie d'usine
     */
    public StrategieUsine getStrategieUsine() {
        return strategieUsine;
    }

    /**
     * obtient l'id unique de l'usine
     * 
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * obtient la position de l'usine
     * 
     * @return position
     */
    public Point getPosition() {
        return position;
    }

    /**
     * change le connecteur extrant de l'usine
     * 
     * @param c le nouveau connecteur
     */
    public void setConnExtrant(Connecteur c) {
        connExtrant = c;
    }

    /**
     * obtient le connecteur extrant de l'usine
     * 
     * @return connExtrant
     */
    public Connecteur getConnExtrant() {
        return connExtrant;
    }

    /**
     * obtient l'observable qui met à jour l'usine
     * 
     * @return prochaineUsine
     */
    public Observable getProchaineUsine() {
        return prochaineUsine;
    }

    /**
     * change la source d'évènement de l'usine
     * 
     * @param prochaineUsine
     */
    public void setProchaineUsine(Observable prochaineUsine) {
        this.prochaineUsine = prochaineUsine;
    }

    /**
     * essaie de trouver la prochaine usine en demandant au connecteur extrant
     * 
     * @return l'usine de fin du connecteur extrant, cast en Observable, si ni l'un
     *         ni l'autre n'est nul
     */
    public Observable rechercheProchaineUsine() {
        if (connExtrant == null)
            return null;
        else
            return connExtrant.getUsineFin();
    }

    /**
     * transmet la liste de composants au connecteur extrant
     * 
     * @param composants liste des composants à livrer
     */
    public void debuterLivraison(Composant[] composants) {
        for (Composant composant : composants) {
            connExtrant.debuterLivraison(composant);
        }
    }

    /**
     * obtient la carte des icones de progression
     * 
     * @return icones
     */
    public Map<String, ImageIcon> getIcones() {
        return icones;
    }

    /**
     * obtient la carte des stocks
     * 
     * @return stock
     */
    public Map<Composant, String> getStock() {
        return stock;
    }

    /**
     * ajoute un composant au stock ou ajuste sa quantite
     * 
     * @param c composant à ajouter
     */
    public void ajouterStock(Composant c) {
        var invLimitant = entrees.getComposant(c.getClass());
        stock.ajouterComposant(invLimitant, c.getQuantite());
    }

    /**
     * obtient la carte des entrees
     * 
     * @return entrees
     */
    public Map<Composant, String> getEntrees() {
        return entrees;
    }

    /**
     * obtient la carte des sorties
     * 
     * @return sorties
     */
    public Map<Composant, String> getSorties() {
        return sorties;
    }

    /**
     * obtient une carte des composants communs aux entrees et au stock, si la
     * quantité dans le stock excède celle de son équivalant dans les entrées
     * 
     * @return les composants qui remplissent la condition
     */
    public HashMap<Composant, Composant> getStockPertinent() {
        var stockPertinent = new HashMap<Composant, Composant>();
        for (Composant e : entrees.keySet()) {

            for (Composant s : stock.keySet()) {
                boolean compTrouve = e.getClass() == s.getClass();
                boolean quantiteSuffisante = compTrouve ? s.getQuantite() >= e.getQuantite() : false;

                if (quantiteSuffisante) {
                    stockPertinent.put(e, s);
                }
            }
        }
        return stockPertinent;
    }

    /**
     * obtient le tableau des composants en frabrication
     * 
     * @return enFabrication
     */
    public Composant[] getEnFabrication() {
        return enFabrication;
    }

    /**
     * change le tableau des composants en fabrication
     * 
     * @param nouv nouveau tableau de Composants
     */
    public void setEnFabrication(Composant[] nouv) {
        enFabrication = nouv;
    }

    /**
     * obtient l'intervalle de production
     * 
     * @return intervalleProduction
     */
    public int getIntervalleProduction() {
        return intervalleProduction;
    }

    /**
     * utilise une node XML pour retrouver et changer l'intervalle de production
     * 
     * @param p node contenant la nouvelle valeur
     */
    public void setIntervalleProduction(Node p) {
        intervalleProduction = Integer.parseInt(p.getTextContent());
    }

    /**
     * obtient le compteur de tours depuis la mise en production de composants
     * 
     * @return compteurTour
     */
    public int getCompteurTour() {
        return compteurTour;
    }

    /** ajoute 1 au compteur de tour */
    public void incrementerCompteurTour() {
        compteurTour++;
    }

    /** réinitialise le compteur de tour à 0 */
    public void demarrerCompteurTour() {
        compteurTour = 0;
    }

    
    /** 
     * @param numPhase
     */
    @Override
    public void executerPhaseTour(int numPhase) {
        strategieUsine.performer(this, numPhase);
    }

    
    /** 
     * @return int
     */
    @Override
    public int getPrioriteDessin() {
        return prioriteDessin;
    }

    
    /** 
     * @param g
     */
    @Override
    public void dessiner(Graphics g) {
        strategieAffichage.dessiner(this, g);
    }

    /**
     * accède à la collection d'icônes et retourne celui qui est le plus près de la
     * fraction de progression
     * 
     * @param fractionProgression fraction de progression
     * @return l'un des icônes d'icones
     */
    public ImageIcon getIconeProgression(float fractionProgression) {
        return icones.getIconeProgression(fractionProgression);
    }

    
    /** 
     * @param copieur
     */
    /**
     * stocke cette instance d'usine comme usine originelle de sa sous-classe, qui
     * peut alors être utilisée pour enregistrer des paramètres communs à toutes les
     * instances de cette sous-classe
     */
    public abstract void setOriginelle();

    
    /** 
     * @param copieur
     * @return Usine
     */
    /**
     * permet de retrouver l'usine originelle d'une sous-classe d'usine, qui peut
     * alors être lue pour retrouver des paramètres communs à toutes les instances
     * de cette sous-classe
     * 
     * @return l'usine originelle de la sous-classe implémentant la méthode
     */
    public abstract Usine getOriginelle();

    /**
     * retrouve l'usine originelle de la sous-classe de copieur et change ses
     * paramètres
     * 
     * @param copieur usine copiant son usine originelle
     */
    public static void copierOriginelle(Usine copieur) {
        Usine originelle = copieur.getOriginelle();

        copieur.entrees = originelle.entrees;
        copieur.sorties = originelle.sorties;
        copieur.strategieUsine = originelle.strategieUsine;
        copieur.strategieAffichage = originelle.strategieAffichage;
        copieur.icones = originelle.icones;
        copieur.intervalleProduction = originelle.intervalleProduction;
    }

    /**
     * retrouve la position de l'usine à l'aide d'une node XML
     * 
     * @param s node dont la position sera extraite
     */
    public void placerSimulation(Node s) {
        NamedNodeMap attr = s.getAttributes();

        Node idNode = attr.getNamedItem(AttributsXML.id.toString());
        if (idNode != null)
            this.id = Integer.parseInt(idNode.getTextContent());

        Node xNode = attr.getNamedItem(AttributsXML.x.toString());
        Node yNode = attr.getNamedItem(AttributsXML.y.toString());
        if (xNode != null && yNode != null)
            position = new Point(Integer.parseInt(xNode.getTextContent()), Integer.parseInt(yNode.getTextContent()));
    }

    
    /** 
     * @param node
     * @param xmlEnum
     * @return boolean
     */
    protected static boolean nodeNameEquals(Node node, Enum<?> xmlEnum) {
        return node.getNodeName().equals(xmlEnum.toString());
    }

    /** énumération des noms de balises servant à configurer les usines */
    protected static enum BalisesXml {
        icones, entree, sortie, intervalleProduction("interval-production");

        String texteBalise;

        BalisesXml() {
            texteBalise = this.name();
        }

        BalisesXml(String texteBalise) {
            this.texteBalise = texteBalise;
        }

        @Override
        public String toString() {
            return texteBalise;
        }
    }

    /** énumérations des attributs servant à configurer les usines */
    protected static enum AttributsXML {
        type, quantite, capacite, id, x, y;
    }

    /**
     * obtient la vitesse que diffusera l'usine à ses observateurs
     * 
     * @return vitesseDiffusee
     */
    public float getVitesseDiffusee() {
        return vitesseDiffusee;
    }

    /**
     * change la vitesse diffusée aux observateurs de l'usine
     * 
     * @param vitesse
     */
    public void setVitesseDiffusee(float vitesse) {
        vitesseDiffusee = vitesse;
    }

    /**
     * obtient l'efficacité de production de l'usine
     * 
     * @return tauxExecution
     */
    public float getTauxExecution() {
        return tauxExecution;
    }

    /**
     * change l'efficacite de production de l'usine
     * 
     * @param nouvTaux
     */
    public void setTauxExecution(float nouvTaux) {
        tauxExecution = nouvTaux;
    }

    
    /** 
     * @param o
     */
    @Override
    public void ajouterObservateur(Observateur o) {
        obs.addPropertyChangeListener(o);
    }

    
    /** 
     * @param o
     */
    @Override
    public void retirerObservateur(Observateur o) {
        obs.removePropertyChangeListener(o);
    }

    
    /** 
     * @param nomPropriete
     * @param vielleVal
     * @param nouvVal
     */
    @Override
    public void notifierObservateurs(String nomPropriete, Object vielleVal, Object nouvVal) {
        obs.firePropertyChange(nomPropriete, vielleVal, nouvVal);
    }

    
    /** 
     * @param evt
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals(UsineEvents.CHANGE_VITESSE.toString()) && evt.getOldValue() instanceof Float
                && evt.getNewValue() instanceof Float) {

            if (tauxExecution == (float) evt.getOldValue())
                tauxExecution = (float) evt.getNewValue();

        } else if (evt.getPropertyName().equals(UsineEvents.CHANGE_VENDEUR.toString())
                && evt.getNewValue() instanceof Vendeur) {
            if (strategieUsine instanceof Vendeur)
                strategieUsine = (StrategieUsine) evt.getNewValue();
        }
    }
}
