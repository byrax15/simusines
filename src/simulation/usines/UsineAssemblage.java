/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: UsineAssemblage.java
Date créé: 2021-05-15
Date dern. modif. 2021-06-06
*******************************************************/

package simulation.usines;

import java.lang.reflect.InvocationTargetException;

import org.w3c.dom.DOMException;
import org.w3c.dom.Node;

import simulation.usines.strategies.AffichageProduction;
import simulation.usines.strategies.Producteur;

/** sous-classe d'usine assemblant des avions */
public class UsineAssemblage extends Usine {

    private static UsineAssemblage originel;

    /** construit une UsineAssemblage avec le constructeur d'usine vide */
    public UsineAssemblage() {
        super();
    }

    /**
     * construit une UsineAssemblage avec le constructeur d'usine avec Node, une
     * stratégie Producteur et une stratégie d'affichage de Production
     * 
     * @param nodeUsine
     * @throws NumberFormatException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     * @throws SecurityException
     * @throws ClassNotFoundException
     * @throws DOMException
     */
    public UsineAssemblage(Node nodeUsine)
            throws NumberFormatException, InstantiationException, IllegalAccessException, IllegalArgumentException,
            InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException, DOMException {
        super(nodeUsine, new Producteur(), new AffichageProduction());
    }

    @Override
    public void setOriginelle() {
        originel = this;
    }

    
    /** 
     * @return Usine
     */
    @Override
    public Usine getOriginelle() {
        return originel;
    }
}
