/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: UsineEvents.java
Date créé: 2021-06-03
Date dern. modif. 2021-06-06
*******************************************************/

package simulation.usines;

/**
 * énumération des évènements auxquels doivent réagir les usines
 */
public enum UsineEvents {
    CHANGE_VITESSE, CHANGE_VENDEUR
}