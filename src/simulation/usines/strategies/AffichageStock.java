/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: AffichageStock.java
Date créé: 2021-05-15
Date dern. modif. 2021-06-06
*******************************************************/

package simulation.usines.strategies;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.util.Map;

import simulation.composants.Composant;
import simulation.usines.Usine;

/**
 * suites d'instructions permettant à une usine d'afficher le taux d'occupation
 * de son stock
 */
public class AffichageStock implements StrategieAffichage {

    @Override
    public void dessiner(Usine u, Graphics g) {
        Float maxOccup = null;
        Map<Composant, String> entrees = u.getEntrees();
        Map<Composant, String> stock = u.getStock();

        for (Composant s : stock.keySet()) {
            for (Composant e : entrees.keySet()) {
                if (s.getClass() == e.getClass()) {

                    float fracOccup = s.getQuantite() / (float) e.getQuantite();
                    if (maxOccup == null)
                        maxOccup = fracOccup;
                    else if (fracOccup > maxOccup)
                        maxOccup = fracOccup;
                }
            }
        }

        if (maxOccup == null)
            maxOccup = 0f;

        Image image = u.getIconeProgression(maxOccup).getImage();

        Point position = u.getPosition();
        Point offset = new Point(position.x - image.getWidth(null) / 2, position.y - image.getHeight(null) / 2);

        g.drawImage(image, offset.x, offset.y, null);
        g.drawString("Vit. exéc. :", offset.x, (int) (position.y + image.getHeight(null)));
        g.drawString((int) (u.getTauxExecution() * 100) + " %", offset.x,
                (int) ((position.y + image.getHeight(null) * 1.5)));
        g.setColor(u.getStrategieUsine() instanceof VendeurAleatoire ? Color.GREEN : Color.BLUE);
        g.drawRect(offset.x, offset.y, image.getWidth(null), image.getHeight(null));
        g.setColor(Color.BLACK);
    }
}
