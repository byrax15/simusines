/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: Producteur.java
Date créé: 2021-05-15
Date dern. modif. 2021-06-06
*******************************************************/

package simulation.usines.strategies;

import java.awt.Point;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.function.Consumer;

import simulation.composants.Composant;
import simulation.usines.Usine;
import simulation.usines.UsineEvents;

/** implémentation de stratégie de production de composants */
public class Producteur implements StrategieUsine {

    private ArrayList<Consumer<Usine>> phases = new ArrayList<Consumer<Usine>>(2);

    /**
     * construit un nouveau producteur : l'usine tente de produire en phase 0,
     * débute une livraison en phase 1 si la production est complète ou avertit les
     * usines qui l'observent de ralentir leur production dans le cas contraire
     */
    public Producteur() {
        phases.add((u) -> {
            Map<Composant, String> sorties = u.getSorties();
            Map<Composant, Composant> stockPertinent;

            if (u.getEnFabrication() == null) {

                stockPertinent = u.getStockPertinent();

                if (stockPertinent.size() == u.getEntrees().size()) {
                    try {
                        u.demarrerCompteurTour();
                        int i = 0;
                        Composant[] enFabrication = new Composant[sorties.size()];
                        for (Composant cle : sorties.keySet()) {
                            enFabrication[i++] = cle.getClass().getConstructor(Point.class, int.class).newInstance(null,
                                    cle.getQuantite());
                        }
                        u.setEnFabrication(enFabrication);
                    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                            | InvocationTargetException | NoSuchMethodException | SecurityException e) {
                        System.out.println("Il manque un constructeur au Composant : \n" + e);
                    }
                }

            } else {
                if (new Random().nextFloat() < u.getTauxExecution())
                    u.incrementerCompteurTour();
            }
        });
        phases.add((u) -> {
            if (u.getEnFabrication() != null && u.getCompteurTour() >= u.getIntervalleProduction()) {
                u.debuterLivraison(u.getEnFabrication());
                u.demarrerCompteurTour();
                u.setEnFabrication(null);
            }

            if (u.getProchaineUsine() == null) {
                var prochaineUsine = u.rechercheProchaineUsine();
                if (prochaineUsine != null) {
                    prochaineUsine.ajouterObservateur(u);
                    u.setProchaineUsine(prochaineUsine);
                }
            }

            if (u.getEnFabrication() != null) {
                var nouvVitDiff = 1 * u.getTauxExecution() * (3 / 4f);
                if (u.getVitesseDiffusee() != nouvVitDiff) {
                    u.notifierObservateurs(UsineEvents.CHANGE_VITESSE.toString(), u.getVitesseDiffusee(), nouvVitDiff);
                    u.setVitesseDiffusee(nouvVitDiff);
                }
            } else {
                var nouvVitDiff = 1 * u.getTauxExecution();
                if (u.getVitesseDiffusee() != nouvVitDiff) {
                    u.notifierObservateurs(UsineEvents.CHANGE_VITESSE.toString(), u.getVitesseDiffusee(), nouvVitDiff);
                    u.setVitesseDiffusee(nouvVitDiff);
                }
            }

        });
    }

    @Override
    public void performer(Usine u, int numPhase) {
        phases.get(numPhase).accept(u);
    }

}
