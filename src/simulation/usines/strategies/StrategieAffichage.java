/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: StrategieAffichage.java
Date créé: 2021-05-15
Date dern. modif. 2021-06-06
*******************************************************/

package simulation.usines.strategies;

import java.awt.Graphics;

import simulation.usines.Usine;

/**
 * interface permettant de changer à la volée la façon dont est représentée une
 * usine
 */
public interface StrategieAffichage {
    public void dessiner(Usine u, Graphics g);
}