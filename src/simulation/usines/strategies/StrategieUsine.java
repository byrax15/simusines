/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: StrategieUsine.java
Date créé: 2021-05-15
Date dern. modif. 2021-06-06
*******************************************************/

package simulation.usines.strategies;

import simulation.usines.Usine;

/**
 * interface premettant de changer à la volée le comportement de l'usine à
 * chaque tour
 */
public interface StrategieUsine {

    /**
     * performe les actions décrites par la stratégie pour la phase donnée
     * 
     * @param u        Usine qui accomplit la stratégie
     * @param numPhase phase du tour à exécuter
     */
    public void performer(Usine u, int numPhase);

}
