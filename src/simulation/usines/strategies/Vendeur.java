/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: Vendeur.java
Date créé: 2021-05-15
Date dern. modif. 2021-06-06
*******************************************************/

package simulation.usines.strategies;

import java.util.ArrayList;
import java.util.function.Consumer;

import simulation.composants.Composant;
import simulation.usines.Usine;
import simulation.usines.UsineEvents;

/**
 * implémentation abstraite d'une stratégie d'usine qui permet de vendre un
 * exemplaire de chaque composant stocké
 */
public abstract class Vendeur implements StrategieUsine {
    protected ArrayList<Consumer<Usine>> phases = new ArrayList<Consumer<Usine>>(2);

    /**
     * construit une stratégie de vente : l'usine tente de vendre en phase 0, et
     * avertit les usines qui l'observe en phase 1
     */
    protected Vendeur() {
        phases.add(u -> {
            if (u.getEnFabrication() == null) {

                var stocks = u.getStock();

                boolean stocksSuffisants = true;
                for (Composant c : stocks.keySet()) {
                    stocksSuffisants = stocksSuffisants && c.getQuantite() >= 1;
                }
                if (stocks.size() > 0 && stocksSuffisants) {
                    u.setEnFabrication(new Composant[0]);
                    u.demarrerCompteurTour();
                }

            } else if (verifierVente(u)) {
                for (Composant c : u.getStock().keySet()) {
                    c.retirerQuantite(1);
                    u.demarrerCompteurTour();
                    u.setEnFabrication(null);
                }
            } else {
                u.incrementerCompteurTour();
            }
        });
        phases.add(u -> {

            var nouvVitDiff = 1 * u.getTauxExecution();
            for (Composant e : u.getEntrees().keySet()) {
                for (Composant s : u.getStock().keySet()) {
                    if (e.getClass() == s.getClass()) {
                        var fracLibre = 1 - s.getQuantite() / (float) e.getQuantite();
                        nouvVitDiff *= fracLibre;
                    }
                }
            }
            u.notifierObservateurs(UsineEvents.CHANGE_VITESSE.toString(), u.getVitesseDiffusee(), nouvVitDiff);
            u.setVitesseDiffusee(nouvVitDiff);
        });
    }

    
    /** 
     * @param u
     * @param numPhase
     */
    @Override
    public void performer(Usine u, int numPhase) {
        phases.get(numPhase).accept(u);
    }

    /**
     * permet aux implémentations concrètes de définir les critères nécessaires pour
     * vendre pendant un tour
     * 
     * @param u l'usine qui tente de vendre
     * @return vrai si l'usine peut vendre pendant ce tour, faux sinon
     */
    public abstract boolean verifierVente(Usine u);
}
