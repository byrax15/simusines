/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: VendeurAleatoire.java
Date créé: 2021-05-15
Date dern. modif. 2021-06-06
*******************************************************/

package simulation.usines.strategies;

import java.util.Random;

import simulation.usines.Usine;

/**
 * sous-classe de vendeur tentant de vendre à chaque tour à raison d'une chance sur
 * x, où x est le taux l'intervalle de production de l'usine
 */
public class VendeurAleatoire extends Vendeur {

    /**
     * construit un VendeurAleatoire en appelant le constructeur vide de Vendeur
     */
    public VendeurAleatoire() {
        super();
    }

    
    /** 
     * @param u
     * @return boolean
     */
    @Override
    public boolean verifierVente(Usine u) {
        return new Random().nextInt(u.getIntervalleProduction()) == 0;
    }
}
