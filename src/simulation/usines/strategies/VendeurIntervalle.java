/******************************************************
Cours:   LOG121
Session: E2021
Groupe:  03
Projet: Laboratoire #1
Étudiant(e)s: Vincent Alix-Joly
              
              
Professeur : Vincent Lacasse
Nom du fichier: VendeurIntervalle.java
Date créé: 2021-05-15
Date dern. modif. 2021-06-06
*******************************************************/

package simulation.usines.strategies;

import simulation.usines.Usine;

/**
 * sous-classe de Vendeur tentant de vendre à chaque x tours où x est
 * l'intervalle de production de l'usine
 */
public class VendeurIntervalle extends Vendeur {

    /**
     * construit un Vendeurintervalle en appelant le constructeur vide de Vendeur
     */
    public VendeurIntervalle() {
        super();
    }

    
    /** 
     * @param u
     * @return boolean
     */
    @Override
    public boolean verifierVente(Usine u) {
        return u.getCompteurTour() >= u.getIntervalleProduction();
    }

}
